

With Pyroomacoustics
all with nfft=256

# Used data
rate1, data1 = wavfile.read('data/fq_sample3_spkr0_angle20.wav')
rate2, data2=wavfile.read('data/fq_sample3_spkr0_angle70.wav')
data = data1[:,indices]+data2[:,indices]

MUSIC estimator with DOA estimations: [18. 68.].
FRIDA estimator with DOA estimations: [24.22612006 79.38195243].
SRP estimator with DOA estimations: [19. 68.].

-> Application also possible to 3D constellations



