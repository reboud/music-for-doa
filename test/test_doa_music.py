#!/usr/bin/env python

from mfdoa.doa_estimators import DoaEstimatorMusic, DoaEstimatorMusicImproved, DoaEvaluator
from mfdoa.generator import Generator

import numpy as np

def test_get_peaks():
    gen = Generator()
    doamusic = DoaEstimatorMusic()

    gen.noise_std_dev = 1
    gen.n_micros = 10
    gen.n_samples = 100
    N_mics = len(gen.mic_doas)
    delta_theta_gt = 10
    delta_f = 0.1
    gen.mic_doas = np.linspace(0,delta_theta_gt*N_mics,N_mics)
    gen.source_freqs = np.linspace(0,delta_f*N_mics,N_mics) + 0.02
    
    data = gen.generate()
    doamusic.load_generator_config(gen)

    Pmusic, pred_angles = doamusic.get_doa_estimate(data)

    assert np.max(pred_angles - gen.mic_doas) < 0.5
