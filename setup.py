#!/usr/bin/env python

import setuptools 

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(name='mfdoa',
      version='0.01',
      description='MUSIC for Direction of Arrival',
      author='Olaf Duenkel, Sandra Gib, Pierre Reboud',
      author_email='olaf.dunkel@epfl.ch, pierre.reboud@epfl.ch',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='https://gitlab.epfl.ch/reboud/music-for-doa',
      package_dir={"": "src"},
      packages=setuptools.find_packages(where="src"),
      python_requires=">=3.7",
     )