# MUSIC for DOA 


## Overleaf Report Page
[Overleaf](https://www.overleaf.com/8856122785tqgkyxvgvskr)


## Getting started

#### Venv Setup
First set up a python virtual environment:
```
$ cd <desired-dir>
$ python3 -m venv <desired venv name>
$ activate desired venv name
```
Make sure to install python 3 in case you do not have it.

#### Module Installation
```
$ cd <desired cloning directory for the mfdoa repo>
$ git clone <repo url>
$ pip install -e .
$ pip install -r requirements.txt
```

#### Usage
Some basic use cases are outlined in the Jupyter Notebook a1_doa_music_demonstration.ipynb.

## Ideas for advanced methods
More advanced data generation and DOA for 3D problem
Estimate the number of microphones automatically

