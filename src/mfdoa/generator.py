import mfdoa
from mfdoa.utils.utils import read_generator_configs, wav_reader

from abc import ABC, abstractmethod
from typing import Dict, List, Tuple, Any
import numpy as np

from scipy.constants import speed_of_light
from scipy.linalg import toeplitz


class Gen(ABC):
    @abstractmethod
    def __init__(self):
        self.load_configs()

    @abstractmethod
    def set_configs(self, **kwargs):
        if kwargs:
            for (k, v) in kwargs.items():
                if v is not None:
                    setattr(self, k, v)

    @abstractmethod
    def load_configs(self):
        configs: Dict[Any, Any] = read_generator_configs()
        for (k, v) in configs.items():
            setattr(self, k, v)

    @abstractmethod
    def generate(self):
        pass


class Generator(Gen):
    def __init__(self):
        super().__init__()

    def load_configs(self) -> None:
        super().load_configs()

    def generate(self) -> np.ndarray:
        super().generate()
        signals: np.ndarray = self._generate_mic_signals()
        return signals

    def set_configs(
        self,
        noise_std_dev: float,
        n_micros: int,
        n_samples: int,
        delta_theta_gt: float,
        delta_f: float,
        doas_deg=None,
        noise_ar1=None,
    ):
        super().set_configs(
            noise_std_dev=noise_std_dev,
            n_micros=n_micros,
            n_samples=n_samples,
            delta_theta_gt=delta_theta_gt,
            delta_f=delta_f,
            doas_deg=doas_deg,
            noise_ar1=noise_ar1,
        )
        n_sources = len(self.doas_deg)
        self.doas_deg = np.linspace(0, delta_theta_gt * n_sources, n_sources)
        self.source_freqs = np.linspace(0, delta_f * n_sources, n_sources) + 0.02

    def _generate_mic_signals(self) -> np.ndarray:
        doas_rad: np.ndarray = np.array(self.doas_deg) / 180 * np.pi
        freqs: np.ndarray = np.array(self.source_freqs) * np.pi
        n_sources: int = len(self.source_freqs)
        d: int = self.between_distance
        wavelen: int = 2 * self.between_distance

        E: np.ndarray = np.exp(
            -1j
            * 2
            * np.pi
            * d
            / wavelen
            * np.kron(np.arange(self.n_micros), np.sin(doas_rad)).reshape(
                (self.n_micros, n_sources)
            )
        )
        x0: np.ndarray = 2 * np.exp(
            1j
            * (
                np.kron(freqs, np.arange(self.n_samples)).reshape(
                    (n_sources, self.n_samples)
                )
            )
        )
        X: np.ndarray = np.dot(E, x0)
        toeplitz_vector: np.ndarray = np.array(
            [self.noise_ar1 ** i for i in range(self.n_micros)]
            )
        Sigma: np.ndarray = toeplitz(toeplitz_vector)
        Y: np.ndarray = X + self.noise_std_dev * Sigma @ np.random.randn(
            self.n_micros, self.n_samples
        )

        return Y

    def read_data(self) -> Tuple[List[int], List[Tuple[int, np.ndarray]]]:
        data: Tuple[List[int], List[Tuple[int, np.ndarray]]] = wav_reader()
        return data
