#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt


class Visualizor:
    def __init__(self) -> None:
        pass


def plot_music_doa_estimation(
    gt_thetas: np.ndarray,
    theta: np.ndarray,
    pred: np.ndarray,
    Pmusic: np.ndarray,
    save_name: str = None,
):

    indx_plot = Pmusic > Pmusic.min()  # +0.15
    plt.plot(theta[indx_plot], Pmusic[indx_plot], "-k")
    plt.xlabel("Angle [deg]")
    plt.ylabel("Estimated spectrum [dB]")
    plt.title("MUSIC for DOA")
    plt.vlines(pred, Pmusic.min() - 2, Pmusic.max() + 2, "g")
    if gt_thetas is not None:
        plt.vlines(gt_thetas, Pmusic.min() - 2, Pmusic.max() + 2, "r")
    if save_name is not None:
        plt.savefig("figures/" + save_name)
    else:
        plt.show()
