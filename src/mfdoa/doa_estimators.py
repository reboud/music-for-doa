#!/usr/bin/env python

__author__ = "Olaf Dünkel"
__email__ = "olaf.dunkel@epfl.ch"

from abc import ABC, abstractmethod
import numpy as np
from typing import Dict, List, Tuple, Any 
from scipy.signal import find_peaks

import matplotlib.pyplot as plt

import pyroomacoustics as pra
from sympy import ShapeError

from mfdoa.utils.utils import read_doa_config, read_generator_configs
from mfdoa.generator import Generator


class DoaEvaluator:
    def __init__(self) -> None:
        pass

    @staticmethod
    def get_mae(pred: np.ndarray, gt: np.ndarray) -> float:
        if len(pred) != len(gt):
            print(f"Not the same number of frequencies considered.")
            gt = np.unique(gt)
            if len(pred) != len(gt):
                if len(np.unique(np.abs(pred))) != len(gt):
                    return -1.0
                else:
                    pred = np.unique(np.abs(pred))
            print(f" -> Applied unique operator gt={gt}, pred={pred}")
        pred, gt = np.sort(pred), np.sort(gt)
        return np.mean(np.abs(pred - gt))

    @staticmethod
    def get_max_SNR(Pmusic: np.ndarray) -> np.ndarray:
        return max(Pmusic) - min(Pmusic)


class DoaEstimator(ABC):
    @abstractmethod
    def __init__(self) -> None:
        pass

    def __call__(self, x: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        return self.get_doa_estimate(x)

    def load_configs(self, file_name: str) -> None:
        configs: Dict[Any, Any] = read_doa_config(file_name)
        for (k, v) in configs.items():
            setattr(self, k, v)

    @abstractmethod
    def get_doa_estimate(self, x: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        pass
        


class DoaEstimatorMusic(DoaEstimator):
    def __init__(self, gen: Generator = None) -> None:
        super().__init__()
        super().load_configs("music_doa.json")
        self.theta: np.ndarray = np.arange(
            self.interval_angle[0], self.interval_angle[1], self.resolution_angle
        )
        self.L: int = np.size(self.theta)

        if gen is not None:
            self.load_generator_config(gen)

        self.pos_micros: np.ndarray = self.dist_micros * np.arange(self.n_micros)

    def get_estimates_by_max_of_Pmusic(self, Pmusic: np.ndarray) -> np.ndarray:
        peaks, _ = find_peaks(Pmusic, height=Pmusic.min())
        peaks = peaks[np.abs(self.theta[peaks]) < 90]
        peaks = peaks[np.argsort(-Pmusic[peaks])]
        peaks = peaks[: self.num_sources]
        while len(peaks) is not self.num_sources:
            peaks = np.insert(peaks, 0, 0)
        pred = self.theta[peaks]
        return pred

    def load_generator_config(self, generator: Generator) -> None:
        self.dist_micros: int = generator.between_distance
        self.wavelen: int = generator.between_distance * 2
        self.n_micros: int = generator.n_micros
        self.num_sources: int = len(generator.source_freqs)
        self.pos_micros = self.dist_micros * np.arange(self.n_micros)

    def get_correlation(self, x0: np.ndarray) -> np.ndarray:
        R = np.dot(x0, x0.conj().T)  # empirical correlation matrix
        return R

    def get_steering_angle(self) -> np.ndarray:
        a: np.ndarray = np.exp(
            -1j
            * 2
            * np.pi
            / self.wavelen
            * np.kron(self.pos_micros, np.sin(self.theta * np.pi / 180)).reshape(
                self.n_micros, self.L
            )
        )
        return a

    def get_doa_estimate(self, x: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        self.L: int = np.size(self.theta)

        x0: np.ndarray = (x.T - x.T.mean(axis=0)).T
        R: np.ndarray = self.get_correlation(x0)

        n, v = np.linalg.eig(R)  # eigenvalue decomposition
        ids: np.ndarray = np.abs(n).argsort()[
            : (self.n_micros - self.num_sources)
        ]  # smallest eignvalues
        En: np.ndarray = v[:, ids]
        R_en: np.ndarray = np.dot(En, En.conj().T)  # Ren = EnEn'

        # Calculate steering angle

        a: np.ndarray = self.get_steering_angle()
        Pmusic: np.ndarray = np.zeros(self.L)

        for i in range(self.L):
            Pmusic[i] = 1 / abs(
                np.dot(a[:, i].conj().T, np.dot(R_en, a[:, i]))
            )  # Pmusic = a(theta)'*En*En'*a(theta)

        pred_angles = self.get_estimates_by_max_of_Pmusic(Pmusic)

        pred_angles = np.sort(pred_angles)

        return Pmusic, pred_angles


class DoaEstimatorMusic2Dmics(DoaEstimatorMusic):
    def __init__(self, gen: Generator = None) -> None:
        super().__init__(gen)

    def get_steering_angle(self) -> np.ndarray:
        a = np.array(
            [np.cos(self.theta / 180 * np.pi), np.sin(self.theta / 180 * np.pi)]
        )
        atheta = np.exp(-1j * 2 * np.pi / self.wavelen * np.dot(self.pos_micros, a))
        return atheta


class DoaEstimatorMusicImproved(DoaEstimatorMusic):
    def __init__(self, gen: Generator = None) -> None:
        super().__init__(gen)

    def get_correlation(self, x0: np.ndarray) -> np.ndarray:

        R = np.dot(x0, x0.conj().T)
        J = np.flip(np.eye(self.n_micros), axis=1)
        R = R + np.dot(J, np.dot(R.conj(), J))

        return R


class DoaEstimatorPryoomacoustics(DoaEstimator):

    def __init__(self,  gen: Generator = None, sampling_frequency: int = 16_000, nfft: int = 256) -> None:
        super().__init__()
        super().load_configs("music_doa.json")
        # self.theta: np.ndarray = np.arange(
        #     self.interval_angle[0], self.interval_angle[1], self.resolution_angle
        # )
        self.theta = np.arange(10, 180)
        self.L: int = np.size(self.theta)
        if gen is not None:
            self.load_generator_config(gen)

        # FIXME This needs to be with the shape of (2,<num_mics>)
        self.pos_micros: np.ndarray = self.dist_micros * np.arange(self.n_micros)
        self.sampling_frequency = sampling_frequency
        self.nfft = nfft

        self.doa_estimator = None
    
    def load_generator_config(self, generator: Generator) -> None:
        self.dist_micros: int = generator.between_distance
        self.wavelen: int = generator.between_distance * 2
        self.n_micros: int = generator.n_micros
        self.num_sources: int = len(generator.source_freqs)
        self.pos_micros = self.dist_micros * np.arange(self.n_micros)

    def get_mae(self, gt: list = None) -> float:
        doa_esimate = self.doa_estimator.azimuth_recon * 180 / np.pi
        return DoaEvaluator.get_mae(doa_esimate,np.degrees(gt))

    def plot_doa_estimate(self, caption: str = None, gt: list = None):
        doa_esimate = self.doa_estimator.azimuth_recon * 180 / np.pi
        if caption is not None:
            if gt is not None:
                print(f"{caption} Estimate: {doa_esimate}. MAE: {DoaEvaluator.get_mae(doa_esimate,np.degrees(gt)):.2f}.")
            else:
                print(f"{caption} Estimate: {doa_esimate}.")
        self.doa_estimator.polar_plt_dirac(azimuth_ref=gt)
        # plt.savefig(f"figures/example_doa_pyroom_{caption[:5]}.png")
        plt.title(caption)
    
    def plot_steering_response_spectra(self, caption: str = "Steering Response Spectrum"):
        azimuth = self.doa_estimator.grid.azimuth * 180 / np.pi
        X,Y = np.meshgrid(self.doa_estimator.freq_hz*1e-3,azimuth)
        Z = self.doa_estimator.Pssl
        plt.contourf(X, Y, Z, 20)
        plt.title(caption)
        plt.ylabel('Azimuth [degrees]')
        plt.xlabel('f [kHz]')
        

class DoaEstimatorPryoomacousticsMUSIC(DoaEstimatorPryoomacoustics):

    def __init__(self, gen: Generator = None, sampling_frequency: int = 16_000, nfft: int = 256, num_src: int = 2, pos_micros: np.ndarray = None, mics_distances_from_source: np.ndarray = None) -> None:
        super().__init__(gen,sampling_frequency, nfft)
        self.pos_micros = pos_micros
        if len(self.pos_micros.shape) != 2:
            raise ShapeError("pos_micros defined in a wrong way.")
        self.num_src = num_src
        self.mics_distances_from_source = mics_distances_from_source
        # self.doa_estimator = pra.doa.music.MUSIC(self.pos_micros, self.sampling_frequency, self.nfft, num_src=self.num_sources, r=mics_distances_from_source)
        self.doa_estimator = pra.doa.algorithms["MUSIC"](self.pos_micros, self.sampling_frequency, self.nfft, num_src=self.num_src) #, r=mics_distances_from_source) #max_four=4,
        self._stft = pra.transform.stft.STFT(self.nfft,hop=self.nfft//2,transform=np.fft.rfft)

    def get_doa_estimate(self, x: np.ndarray, freq_range: Tuple[float, float] = (500, 4_000.0)) -> Tuple[np.ndarray, np.ndarray]:
        '''
        spec: M x F x S = mics x (nfft/2+1) x snapshots >> M
        '''
        spec = np.array([self._stft.analysis(x[:,i]).T for i in range(x.shape[1])])
        freq_range = list(freq_range)
        self.doa_estimator.locate_sources(spec, freq_range=freq_range)

    def return_doa_estimates(self) -> Tuple[np.ndarray,np.ndarray,np.ndarray]:
        pseudo_spectrum = self.doa_estimator.Pssl
        freqs = self.doa_estimator.freq_bins/self.doa_estimator.nfft*self.doa_estimator.fs
        azimuth = self.doa_estimator.grid.azimuth * 180 / np.pi
        est_doa = self.doa_estimator.azimuth_recon * 180 / np.pi 
        return (pseudo_spectrum, freqs, azimuth, est_doa)

class DoaEstimatorPryoomacousticsFRIDA(DoaEstimatorPryoomacoustics):

    def __init__(self, gen: Generator = None, sampling_frequency: int = 16_000, nfft: int = 256, num_src: int = 2, pos_micros: np.ndarray = None, mics_distances_from_source: np.ndarray = None) -> None:
        super().__init__(gen,sampling_frequency, nfft)
        self.pos_micros = pos_micros
        if len(self.pos_micros.shape) != 2:
            raise ShapeError("pos_micros defined in a wrong way.")
        self.num_src = num_src
        self.mics_distances_from_source = mics_distances_from_source
        self.doa_estimator = pra.doa.algorithms["FRIDA"](self.pos_micros, self.sampling_frequency, self.nfft, num_src=self.num_src,max_four=4) #, r=mics_distances_from_source) #,
        self._stft = pra.transform.stft.STFT(self.nfft,hop=self.nfft//2,transform=np.fft.rfft)

    def get_doa_estimate(self, x: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        '''
        spec: M x F x S = mics x (nfft/2+1) x snapshots >> M
        '''
        # spec = np.array([pra.stft(s, self.nfft, self.nfft // 2, transform=np.fft.rfft).T for s in x.T])
        spec = np.array([self._stft.analysis(x[:,i]).T for i in range(x.shape[1])])
        self.doa_estimator.locate_sources(spec,freq_range=[500, 4_000.0])
    
    def return_doa_estimates(self) -> Tuple[np.ndarray,np.ndarray,np.ndarray]:
        freqs = self.doa_estimator.freq_bins/self.doa_estimator.nfft*self.doa_estimator.fs
        azimuth = self.doa_estimator.grid.azimuth * 180 / np.pi
        est_doa = self.doa_estimator.azimuth_recon * 180 / np.pi 
        return (freqs, azimuth, est_doa)

class DoaEstimatorPryoomacousticsSRP(DoaEstimatorPryoomacousticsFRIDA):
    def __init__(self, gen: Generator = None, sampling_frequency: int = 16_000, nfft: int = 256, num_src: int = 2, pos_micros: np.ndarray = None, mics_distances_from_source: np.ndarray = None) -> None:
        super().__init__(gen, sampling_frequency, nfft, num_src, pos_micros, mics_distances_from_source)
        self.doa_estimator = pra.doa.algorithms["SRP"](self.pos_micros, self.sampling_frequency, self.nfft, num_src=self.num_src,max_four=4) 