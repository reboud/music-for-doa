import json
import mfdoa
from os.path import join, split
from typing import Dict, List, Tuple, Any
from scipy.io.wavfile import read
import numpy as np

from scipy.signal import find_peaks


def read_generator_configs() -> Dict[Any, Any]:
    path: str = join(mfdoa.__path__[0], "configs", "generation.json")
    with open(path, "r") as j:
        configs: Dict[Any, Any] = json.load(j)
    return configs


def read_doa_config(file_name: str) -> Dict[Any, Any]:
    path: str = join(mfdoa.__path__[0], "configs", file_name)
    with open(path, "r") as j:
        configs: Dict[Any, Any] = json.load(j)
    return configs


def wav_reader() -> Tuple[List[int], List[Tuple[int, np.ndarray]]]:
    base_path: str = join(split(split(mfdoa.__path__[0])[0])[0], "data", "")
    doa_angles: List[int] = [20, 30, 70]
    filenames: List[str] = [
        base_path + f"fq_sample3_spkr0_angle{str(angle)}.wav" for angle in doa_angles
    ]
    wav_data: Tuple[List[int], List[Tuple[int, np.ndarray]]] = (doa_angles, [])
    for name in filenames:
        wav_data[1].append(read(name))
    return wav_data


def get_log_power_values(x: np.ndarray) -> np.ndarray:
    return 10 * np.log10(x / np.max(x))

def get_m_largest_peaks(x: np.ndarray, M: int = 1) -> np.ndarray:
    peaks, _ = find_peaks(x, height=x.min()+1, distance=2)
    peaks = peaks[np.argsort(-x[peaks])]
    peaks = peaks[: M]
    return peaks